"""
This is just a quick quick app I put together to watch email transactions on
a computer and see what I could see. Mostly it is compiled from examples on
the pcapy and impacket site:

    http://oss.coresecurity.com/projects/pcapy.html
    http://oss.coresecurity.com/projects/impacket.html

"""
import os
import sys
import argparse
from datetime import datetime
from pcapy import findalldevs
from pcapy import open_live
from impacket import ImpactDecoder

SMTP = 'SMTP'
IMAP = 'IMAP'
SSMTP = 'SMTP'
IMAP_SSL = 'IMAP'

FILTER_IP = '192.'
FILTER_DEVICE = 'en0'

ports = {
    25: SMTP,
    2525: SMTP,
    465: SSMTP,
    143: IMAP,
    993: IMAP_SSL
    }


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip-filter', type=str, default='')
    parser.add_argument('--device', type=str, default='')
    parser.add_argument('--log-file', type=str, default='.')
    parser.add_argument('--list-devices', action='store_true', default=False)
    return parser.parse_args()


def get_interface():
    ifs = findalldevs()
    for i in xrange(len(ifs)):
        print ifs[i]
    return ifs

def select_interface(ifs):
    has_made_selection = False
    device_id = None
    while not has_made_selection:
        for i in xrange(len(ifs)):
            print '{0}/ {1}'.format(i, ifs[i])
        device_no = raw_input('\nselect a device to use: ')
        if len(ifs) <= device_no >= 0:
            device_id = ifs[i]
            has_made_selection = True
    return device_id

def sniff(interface, filter, log_file):
    print "listening on: {0}".format(interface)
    reader = open_live(interface, 1500, 0, 100)
    reader.setfilter('ip proto \\tcp')
    handler = PacketHandler(filter, log_file)
    try:
        reader.loop(0, handler)
    except KeyboardInterrupt:
        print 'sniffer stopped'
        sys.exit(0)
    finally:
        handler.close()


class PacketHandler(object):

    def __init__(self, ip_filter, log_file):
        self.ip_filter = ip_filter
        log_file = os.path.realpath(os.path.expanduser(log_file))
        self.log_dir = os.path.dirname(log_file)
        self.log_file = open(log_file, 'a+')

    def log(self, message):
        self.log_file.write(message)        
        self.log_file.write('\n')

    def close(self):
        if self.log_file:
            self.log_file.close()

    def __call__(self, hdr, data):
        decoder = ImpactDecoder.EthDecoder()
        ether = decoder.decode(data)

        iphdr = ether.child()
        tcphdr = iphdr.child()

        # i/o ip addresses
        src_ip = iphdr.get_ip_src()
        dst_ip = iphdr.get_ip_dst()
        # i/o ports
        sport = tcphdr.get_th_sport()
        dport = tcphdr.get_th_dport()

        if dport in ports or sport in ports and src_ip.startswith(self.ip_filter):
            current_time = datetime.now()
            message = "{5} {4} {0}:{2} -> {1}:{3}".format(src_ip, dst_ip, sport, dport, ports.get(dport, '?'), current_time)
            print message 
            self.log(message)

            if dport in (25, 2525):
                print 'EMAIL SENDING...'
                message = ''
                try:
                    email_name = '{0}-{1}.txt'.format(current_time.strftime('%Y-%M-%d:%m:%s'), dst_ip).replace(' ', '_')
                    message = 'getting packet data...'
                    packet = tcphdr.get_packet()
                    message += '\nchecking for Content-Type...'
                    if 'Content-Type' in packet:
                        print packet
                        message += '\nwriting packet content to file...'
                        with open(os.path.join(self.log_dir, email_name), 'w') as fh_:
                            fh_.write(packet)
                except Exception as ex:
                    message += '\nERROR READING PACKET: ' + str(ex)
                    print message
                    self.log(message)


if __name__ == '__main__':
    args = get_args()

    FILTER_IP = args.ip_filter
    FILTER_DEVICE = args.device
    LOG_FILE = 'netsniff.log'
    if args.log_file == '.':
        LOG_FILE = os.path.join(args.log_file, LOG_FILE)

    if args.list_devices:
        get_interface()
        sys.exit(0)

    try:
        if FILTER_DEVICE == '':
            FILTER_DEVICE = select_interface(get_interface())
        sniff(FILTER_DEVICE, FILTER_IP, LOG_FILE)
    except Exception as ex:
        print "ERROR: {0}".format(ex)
        sys.exit(1)
